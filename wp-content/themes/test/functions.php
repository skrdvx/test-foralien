<?php
/**
 * Theme functions
 */

require_once __DIR__ . '/vendor/autoload.php';

require get_template_directory() . '/inc/post-types.php';
require get_template_directory() . '/inc/block-types.php';
require get_template_directory() . '/inc/field-groups.php';


function test_scripts() {
    wp_enqueue_style( 'test-css', get_template_directory_uri() . '/dist/css/main.css' );
    wp_enqueue_script( 'test-js', get_template_directory_uri() . '/dist/js/bundle.js', null, null, true);
}
add_action( 'wp_enqueue_scripts', 'test_scripts' );