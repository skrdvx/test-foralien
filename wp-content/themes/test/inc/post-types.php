<?php

function person_register_post_type() {
    $args = array(
        'public'    => true,
        'label'     => __( 'Persons', 'test' ),
        'menu_icon' => 'dashicons-groups',
        'show_in_rest' => true,
        'supports' => array( 'title', 'editor')
    );
    register_post_type( 'person', $args );
}
add_action( 'init', 'person_register_post_type' );