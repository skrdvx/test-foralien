<?php
  acf_register_block_type(array(
    'name'              => 'person',
    'title'             => __('Person'),
    'description'       => __('Person block.'),
    'category'          => 'formatting',
    'keywords'          => array( 'person' ),
    'icon'              => 'admin-comments',
    'render_template'   => 'template-parts/blocks/person.php',
    'enqueue_style'     => get_template_directory_uri() . '/assets/scss/blocks/person.scss',
  ));
