<?php
  acf_register_block_type(array(
    'name'              => 'team',
    'title'             => __('Team'),
    'description'       => __('Team block.'),
    'category'          => 'formatting',
    'keywords'          => array( 'team' ),
    'icon'              => 'admin-comments',
    'render_template'   => 'template-parts/blocks/team.php',
  ));
