<?php
/**
 * Register block types
 */

add_action('acf/init', 'test_init_block_types');
function test_init_block_types() {

  if( function_exists('acf_register_block_type') ) {
    require get_template_directory() . '/inc/blocks/person.php';
    require get_template_directory() . '/inc/blocks/team.php';
  }

}