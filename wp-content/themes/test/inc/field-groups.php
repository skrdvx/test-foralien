<?php
/**
 * ACF Block fields
 */

ACFComposer\ACFComposer::registerFieldGroup([
    'name' => 'person',
    'title' => 'Person',
    'style' => 'seamless',
    'fields' => [
        [
            'label' => 'Image',
            'name' => 'image',
            'aria-label' => '',
            'type' => 'image',
            'instructions' => '',
            'return_format' => 'url',
            'preview_size' => 'thumbnail',
            'library' => 'all',
            'required' => 0,
        ],
        [
            'label' => 'Description',
            'name' => 'description',
            'aria-label' => '',
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
        ],
        [
            'label' => 'Email',
            'name' => 'email',
            'aria-label' => '',
            'type' => 'email',
            'instructions' => '',
            'required' => 0,
        ],
        [
            'label' => 'Blocks',
            'name' => 'blocks',
            'aria-label' => '',
            'type' => 'repeater',
            'instructions' => '',
            'required' => 0,
            'layout' => 'block',
            'pagination' => 0,
            'min' => 0,
            'max' => 0,
            'collapsed' => '',
            'button_label' => 'Add Block',
            'rows_per_page' => 20,
            'sub_fields' => [
                [
                    'label' => 'Title',
                    'name' => 'title',
                    'aria-label' => '',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                ],
                [
                    'label' => 'Text',
                    'name' => 'text',
                    'aria-label' => '',
                    'type' => 'textarea',
                    'instructions' => '',
                    'required' => 0,
                ],
            ],
        ],
        [
            'label' => 'Accordion',
            'name' => 'accordion',
            'aria-label' => '',
            'type' => 'repeater',
            'instructions' => '',
            'required' => 0,
            'layout' => 'block',
            'pagination' => 0,
            'min' => 0,
            'max' => 0,
            'collapsed' => '',
            'button_label' => 'Add Item',
            'rows_per_page' => 20,
            'sub_fields' => [
                [
                    'label' => 'Title open',
                    'name' => 'title_open',
                    'aria-label' => '',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                ],
                [
                    'label' => 'Title close',
                    'name' => 'title_close',
                    'aria-label' => '',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                ],
                [
                    'label' => 'Text',
                    'name' => 'text',
                    'aria-label' => '',
                    'type' => 'textarea',
                    'instructions' => '',
                    'required' => 0,
                ],
            ],
        ],
    ],
    'location' => [
        [
            [
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'person'
            ],
        ],
    ]
]);

ACFComposer\ACFComposer::registerFieldGroup([
    'name' => 'team',
    'title' => 'Team',
    'style' => 'seamless',
    'fields' => [
        [
            'label' => 'Team',
            'name' => 'team',
            'aria-label' => '',
            'type' => 'post_object',
            'instructions' => '',
            'required' => 0,
            'post_type' => array(
                0 => 'person',
            ),
            'post_status' => '',
            'taxonomy' => '',
            'return_format' => 'object',
            'multiple' => 1,
            'allow_null' => 0,
            'ui' => 1,
        ],
    ],
    'location' => [
        [
            [
                'param' => 'block',
                'operator' => '==',
                'value' => 'acf/team'
            ],
        ],
    ]
]);
