
const items = document.querySelectorAll('[data-item="item"]')


if (items) {
    items.forEach(item => {
        const header = item.querySelector('[data-item="header"]')

        header.addEventListener('click', () => {
            item.classList.toggle('is-active');
        })
    });
}