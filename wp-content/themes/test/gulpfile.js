const {
	src,
	dest,
	parallel,
	series,
	watch
} = require('gulp')

const uglify = require('gulp-uglify-es').default
const browserify = require('browserify')
const vinylSource = require('vinyl-source-stream')
const buffer = require('vinyl-buffer')
const log = require('gulplog')
const rename = require('gulp-rename')
const sass = require('gulp-sass')
const sourcemaps = require('gulp-sourcemaps')
const autoprefixer = require('gulp-autoprefixer')
const cssnano = require('gulp-cssnano')
const clean = require('gulp-clean')
const svgSprite = require('gulp-svg-sprite')
const changed = require('gulp-changed')
const fs = require('fs')
const twig = require('gulp-twig')
const data = require('gulp-data')
const htmlBeautify = require('gulp-html-beautify')
const browserSync = require('browser-sync').create()

const BASIC_PATHS = {
	public: './dist',
	src: './assets',
	maps: './maps'
}

const SRC_PATHS = {
	font: `${BASIC_PATHS.src}/font/**/*`,
	scss: `${BASIC_PATHS.src}/scss/`,
	js: `${BASIC_PATHS.src}/js/`
}

const PUBLIC_PATHS = {
	font: `${BASIC_PATHS.public}/font/`,
	css: `${BASIC_PATHS.public}/css/`,
	js: `${BASIC_PATHS.public}/js/`
}

function clear() {
	return src(`${BASIC_PATHS.public}/*`, {
		read: false
	})
		.pipe(clean())
}

function js() {
	const source = `${SRC_PATHS.js}main.js`
	const b = browserify({
		entries: source,
		debug: true
	})
		.transform('babelify', {
			presets: [
				['@babel/preset-env', {
					'targets': {
						'browsers': [ 'last 1 version', 'ie >= 11' ]
					}
				}],
			],
			plugins: ['@babel/plugin-transform-runtime']
		})

	return b.bundle()
		.pipe(vinylSource('bundle.js'))
		.pipe(buffer())
		.pipe(sourcemaps.init({
			loadMaps: true
		}))
		.on('error', log.error)
		.pipe(sourcemaps.write('.'))
		.pipe(dest(PUBLIC_PATHS.js))
		.pipe(browserSync.stream())
}

function minJs() {
	const source = `${PUBLIC_PATHS.js}bundle.js`

	return src(source)
		.pipe(rename({
			extname: '.min.js'
		}))
		.pipe(uglify())
		.pipe(dest(PUBLIC_PATHS.js))
}

function css() {
	const source = `${SRC_PATHS.scss}main.scss`

	return src(source)
		.pipe(changed(PUBLIC_PATHS.css))
		.pipe(sourcemaps.init())
		.pipe(sass())
		.pipe(autoprefixer({
			overrideBrowserslist: ['last 2 versions'],
			grid: true,
			cascade: false
		}))
		.pipe(sourcemaps.write(BASIC_PATHS.maps))
		.pipe(dest(PUBLIC_PATHS.css))
		.pipe(browserSync.stream())
}

function minCss() {
	const source = `${PUBLIC_PATHS.css}main.css`

	return src(source)
		.pipe(rename({
			extname: '.min.css'
		}))
		.pipe(cssnano())
		.pipe(dest(PUBLIC_PATHS.css))
}

function copyFonts() {
	return src([SRC_PATHS.font])
		.pipe(dest(PUBLIC_PATHS.font))
}

function watchFiles() {
	watch(SRC_PATHS.scss, series(css, browserSyncReload))
	watch(SRC_PATHS.js, series(js, browserSyncReload))
	watch(SRC_PATHS.font, series(copyFonts, browserSyncReload))
}

function browserSyncInit(doneCallback) {
	browserSync.init({
		server: {
			baseDir: BASIC_PATHS.public
		},
		host: 'localhost',
		port: 80
	})

	doneCallback()
}

function browserSyncReload(doneCallback) {
	browserSync.reload()
	doneCallback()
}

const build = series(clear, parallel(js, css, copyFonts))
const watching = parallel(build, watchFiles)
const publish = series(parallel(minCss, minJs))

exports.build = build
exports.watch = watching
exports.publish = publish
