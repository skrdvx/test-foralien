<?php
/**
 * Site Header
 */
?>
<header class="header">
  <h1 class="header__title">
    <?php the_title(); ?>
  </h1>
</header>