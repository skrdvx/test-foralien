## Setup
To start Docker from project root folder:
```
docker-compose up -d
```

To install dependencies from theme folder:

```
composer install
npm install
```

## Theme
1. Frontend  
_Gulp vs Webpack_  
2. ACF PRO  
_ACF Composer vs Local JSON_  
3. ACF Blocks  
_ACF Block vs Native way_  
_SCSS vs 'enqueue_style'_  
4. Timber  
_Timber vs PHP templates_  

## Time
+ clean theme = 1h
+ clean styles = 30m
+ setup project = 20m
+ add post type = 20m
+ create blocks = 50m
+ update fields = 30m
+ update templates = 90m
+ update content = 30m
+ update styles = 2h
+ update scripts = 30m

Total: 8h
